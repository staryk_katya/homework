﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4._1
{
    public class Student
    {
        public string FullName { get; set; }

        public int YearOfBirthday { get; set; }

        public string Adress { get; set; }

        public int SchoolNumber { get; set; }

        public Student()
        {

        }
        public Student(string fullName,string adress,  int schoolNumber, int yearOfBirthday )
        {
      
            this.FullName = fullName;
            this.YearOfBirthday = yearOfBirthday;
            this.Adress = adress;
            this.SchoolNumber = schoolNumber;
        }
    }
}
