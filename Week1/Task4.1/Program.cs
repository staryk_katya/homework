﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4._1
{
    class Program
    {
        static List<Student> students = new List<Student>()
        { new Student("Ivan Petrov", "Kyiv", 1, 1990 ),
        new Student ( "Ivan Ivanov", "Kremenchug", 2, 1991),
         new Student ("Ivan Ivanov", "Kremenchug", 3, 1992) ,
         new Student ( "Ivan Ivanov", "Kremenchug", 2, 1989) ,
         new Student ("Ivan Ivanov", "Kremenchug", 4, 1993) };

        static void Main(string[] args)
        {
            Console.WriteLine("1-add student; 2-show all student; 3-show filtered&sorted");
            var answer = Console.ReadLine();
            switch (answer)
            {
                case "1":
                    AddStudent();
                    break;
                case "2":
                    ShowAll();
                    break;
                case "3":
                    ShowBySchool();
                    break;
                default:
                    break;
            }
        }

        private static void ShowBySchool()
        {
            Console.WriteLine("Enter #school");
            int number = Convert.ToInt32(Console.ReadLine());

            foreach (var student in students.Where(student => student.SchoolNumber == number).OrderBy(student => student.YearOfBirthday))
            {
                Console.WriteLine(student.FullName + " school# " + student.SchoolNumber + " " + student.YearOfBirthday);
            }

            //foreach (var student in students)
            //{
            //    if (student.SchoolNumber == number)
            //    {
            //        Console.WriteLine(student.FullName + " school# " + student.SchoolNumber + " " + student.YearOfBirthday);
            //    }
            //}

        }

        private static void ShowAll()
        { 
            foreach (var student in students)
            {
                Console.WriteLine(student.FullName+" school# "+student.SchoolNumber+" "+student.YearOfBirthday);
            }

        }

        private static void AddStudent()
        {
            Console.WriteLine("Enter student's data");
            Student student = new Student();
            Console.WriteLine("Enter full name");
            student.FullName = Console.ReadLine();
            Console.WriteLine("Enter year of birthday");
            student.YearOfBirthday = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter student's address");
            student.Adress = Console.ReadLine();
            Console.WriteLine("Enter school number");
            student.SchoolNumber = Convert.ToInt32(Console.ReadLine());

            students.Add(student);
        }
    }
}
