﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Song
    {
        private string singer;
        private string record;
        public string Singer { get; set; }
        public string Record { get; set; }

        public Song(string singer, string record)
        {
            this.Singer = singer;
            this.Record = record;

        }
    }
}
