﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            string str="";
            
            Console.WriteLine(str);
            Console.Read();


            //// запись в файл
            using (FileStream fstream = new FileStream(@"E:\note.txt", FileMode.OpenOrCreate))
            {
                //    // преобразуем строку в байты
                byte[] array = Encoding.Default.GetBytes(str);




                //    // запись массива байтов в файл
                fstream.Write(array, 0, array.Length);
                Console.WriteLine("Текст записан в файл");
            }

            string result = "";
                //// чтение из файла
               using (FileStream fstream = File.OpenRead(@"E:\note.txt"))
                {
                    // преобразуем строку в байты
                    byte[] array = new byte[fstream.Length];
                    // считываем данные
                    fstream.Read(array, 0, array.Length);
                                                 
                // декодируем байты в строку
                    string textFromFile = Encoding.Default.GetString(array);
                //  Console.WriteLine("Текст из файла: {0}", textFromFile);

                // конвертируем строку в числа
                int [] massive = Regex.Matches(textFromFile, "\\d+").Cast<Match>().Select(x => int.Parse(x.Value))
                .ToArray();

                for (int i = 0; i < massive.Length; i++)
                {
                    massive[i] = Convert.ToInt32(Math.Pow(massive[i], 2));
                    
                }
                Console.WriteLine("Квадрат чисел:");
                for (int i = 0; i < massive.Length; i++)
                {
                    
                     Console.Write("{0} ", massive[i]);
                    result = result + " " + massive[i];
                }
                Console.WriteLine("\n");
            }



            //// запись в файл
            using (FileStream fstream = new FileStream(@"E:\result.txt", FileMode.OpenOrCreate))
            {


                //    // преобразуем строку в байты
                byte[] array = Encoding.Default.GetBytes(result);




                //    // запись массива байтов в файл
                fstream.Write(array, 0, array.Length);
                Console.WriteLine("Текст записан в файл");
            }

            //Console.ReadLine();

        }
    }
    }
