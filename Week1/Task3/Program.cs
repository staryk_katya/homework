﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        //static void QuickSort(int[] a, int l, int r)
        //{
        //    int temp;
        //    int x = a[l + (r - l) / 2];
        //    int i = l;
        //    int j = r;
        //    while (i <= j)
        //    {
        //        while (a[i] < x) i++;
        //        while (a[j] > x) j--;
        //        if (i <= j)
        //        {
        //            temp = a[i];
        //            a[i] = a[j];
        //            a[j] = temp;
        //            i++;
        //            j--;
        //        }
        //    }
        //    if (i < r)
        //        QuickSort(a, i, r);

        //    if (l < j)
        //        QuickSort(a, l, j);
        //}

        static void BubbleSort(int[] array)
        {
            int temp;
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] > array[j])
                    {
                        temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }


        static void Main(string[] args)
        {
            int size = 0;


            Console.WriteLine("Введите размер массива:");
            size = Convert.ToInt32(Console.ReadLine());
            int[] array = new int[size];
            Random random = new Random();
            for (int i = 0; i < size; i++)
            {
                array[i] = random.Next(-500, 500);
                Console.WriteLine(array[i]);
            }

            BubbleSort(array);
            //QuickSort(array,0,size-1);

            for (int i = 0; i < size; i++)
            {
                Console.WriteLine(array[i]);
            }
            Console.ReadLine();
        }
    }
}
