﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task10
{
    class Program
    {
        static string path;
        static string filter;

        private static void GetParametr()
        {
            Console.WriteLine("Введите путь где необходимо осуществлять мониторинг");
            path = Console.ReadLine();

            Console.WriteLine("Введите тип файлов которые необходимо отслеживать ");
            filter = Console.ReadLine();

        }

        static void Main(string[] args)
        {
            GetParametr();
            Logger logger = new Logger(path, filter);


            ConsoleKeyInfo keyStart;
            Console.Write(" Press F for Start ");

            do
            {
                Console.WriteLine();
                keyStart = Console.ReadKey();
            }
            while (keyStart.Key != ConsoleKey.F);
            Thread loggerThread = new Thread(new ThreadStart(logger.Start));
            loggerThread.Start();

            Console.WriteLine();
            ConsoleKeyInfo key;
            Console.Write(" Press Esc for Stop ");

            do
            {
                Console.WriteLine();
                key = Console.ReadKey();
            }
            while (key.Key != ConsoleKey.Escape);
            logger.Stop();

        }
    }
}

