﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11
{
    class Program
    {
       
        static void Main(string[] args)
        {
            Student s1 = new Student {Age=1, FirstName="Tom", LastName="Soyer" };
            Teacher s2 = new Teacher {Age=2, FirstName="Tom", LastName= "Bill" };

            if ( s1 == s2)
                Console.WriteLine("Объекты равны");
            else
            {
                Console.WriteLine("Не равны");
            }

            Console.Read();
        }
    }
}
