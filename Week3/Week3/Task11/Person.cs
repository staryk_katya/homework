﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task11
{
    class Person
    {

       
        public byte Age { get; set; }
        public string FirstName{ get; set; }
        public string LastName { get; set; }

        

       
       
    }

    class Student : Person
    {

        public string Teacher { get; set; }
        public string Gruppa { get; set; }
        public byte Kurs { get; set; }

         
        
    }

    class Teacher : Person
    {

        public static bool operator == (Student s1, Teacher s2)

        {
            if (s1.LastName == s2.LastName && s1.FirstName == s2.FirstName)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool operator !=(Student s1, Teacher s2)

        {
            if (s1.LastName != s2.LastName && s1.FirstName != s2.FirstName)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
