﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class State
    {
        public string Name { get; set; } // название
        public int Population { get; set; } // население
        public double Area { get; set; } // площадь
    }

        class Program
    {
        static void Main(string[] args)
        {
            State s1 = new State { Name = "State1", Area = 300, Population = 100 };

            State s2 = new State { Name = "State2", Area = 200, Population = 70 };
        }
    }
}
