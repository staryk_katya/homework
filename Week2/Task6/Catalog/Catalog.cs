﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    static class Catalog
    {
        private static List<Disk> Disks { get; set; }

        public static int Count {
            get {

                return Disks.Count;
            }
        }

        static Catalog()
        {
            Disks = new List<Disk>();
            Disk disk = new Disk("OE-Model");
            Song song1 = new Song("Vakarchuk S", "911");
            Song song2 = new Song("Vakarchuk S", "Model");
            disk.Songs.Add(song1);
            disk.Songs.Add(song2);
            Disks.Add(disk);
        }

        public static List<Disk> ShowAllDisks()
        {
            return Disks;
        }

        public static List<Song> ShowSongBySinger(string name)
        {
            List<Song> songs = new List<Song>();
            foreach (var disk in Disks)
            {
                foreach (var song in disk.Songs)
                {
                    if (song.Singer == name)
                    {
                        songs.Add(song);
                    }
                }
            }
            return songs;
        }
    }
}
