﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Song
    {
        public string Singer { get; set; }
        public string Name { get; set; }

        public Song(string singer, string name)
        {
            this.Singer = singer;
            this.Name = name;
        }
    }
}
