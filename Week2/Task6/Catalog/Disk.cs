﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Disk
    {
        public string Name { get; set; }

        public List<Song> Songs { get; set; }

        public Disk(string name)
        {
            Songs = new List<Song>();
            Name = name;
        }

        public override string ToString()
        {
            return String.Format("NAME:{0} Count:{1}", Name, Songs.Count);
        }
    }
}
