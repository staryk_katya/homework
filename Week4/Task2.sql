use Northwind

--1
select ProductName, UnitPrice
from Products
where UnitPrice between 15 and 30

--2
select ProductName
from Products
where ProductName like 'C%'

--3
select ProductName
from Products
where ProductName like '_b%'

--4
select ProductName
from Products
where ProductName like '%eso%'

--5
select ProductName
from Products
where ProductName like '%es'
  
--6
select (select ProductID from Products
		where Products.ProductID=[Order Details].ProductID) AS ProdID,
		(Select ProductName from Products
		where Products.ProductID=[Order Details].ProductID) AS ProductName, Quantity, UnitPrice
															--(Quantity *  UnitPrice ) as TotalPrice 
		from [Order Details]


SELECT Products.ProductID, ProductName, Products.UnitPrice, Quantity
   	  FROM Products
INNER JOIN [Order Details]
      ON Products.ProductID = [Order Details].ProductID 

--7
SELECT Products.ProductID, ProductName, Products.UnitPrice, Quantity, CustomerID, OrderDate			
   	  FROM Orders
	  join [Order Details]
	  on Orders.OrderID=[Order Details].OrderID
      JOIN Products
      ON Products.ProductID = [Order Details].ProductID

--8
SELECT Products.ProductID, ProductName, Products.UnitPrice, Quantity, CustomerID, OrderDate			
   	  FROM Orders
	  join [Order Details]
	  on Orders.OrderID=[Order Details].OrderID
      JOIN Products
      ON Products.ProductID = [Order Details].ProductID
order by ProductName

--9
SELECT Products.ProductID, ProductName, Products.UnitPrice, Quantity, CustomerID,  OrderDate		
   	  FROM Orders
	  join [Order Details]
	  on Orders.OrderID=[Order Details].OrderID
      JOIN Products
      ON Products.ProductID = [Order Details].ProductID
Where DATEPART(YEAR, OrderDate) in 
            (Select DATEPART(YEAR, OrderDate) from Orders
			  where (DATEPART(YEAR, OrderDate) =1997))	
order by  UnitPrice desc
		
--10
SELECT Products.ProductID, ProductName, Products.UnitPrice, Quantity, CustomerID,  OrderDate		
   	  FROM Orders
	  join [Order Details]
	  on Orders.OrderID=[Order Details].OrderID
      JOIN Products
      ON Products.ProductID = [Order Details].ProductID
Where DATEPART(YEAR, OrderDate) in 
            (Select DATEPART(YEAR, OrderDate) from Orders
			  where (DATEPART(YEAR, OrderDate) <1997))	
 order by  UnitPrice desc
		
--11
select top 7 Products.ProductID, ProductName, Products.UnitPrice, OrderDate, CustomerID		
   	  FROM Orders
	  join [Order Details]
	  on Orders.OrderID=[Order Details].OrderID
      JOIN Products
      ON Products.ProductID = [Order Details].ProductID
 order by  UnitPrice 
		
--12
Select ProductName, Sum([Order Details].UnitPrice*Quantity) as Summa
from Products
Inner Join [Order Details]
On Products.ProductID=[Order Details].ProductID
Group by Products.ProductName
order by Summa desc 

--13
Select ProductName, Count ([Order Details].OrderID) as Kol
from Products
Inner Join [Order Details]
On Products.ProductID=[Order Details].ProductID
Group by Products.ProductName
order by Kol desc 

--14
SELECT ProductName, Sum([Order Details].UnitPrice*Quantity) as Summa
   	  FROM Orders
	  join [Order Details]
	  on Orders.OrderID=[Order Details].OrderID
      JOIN Products
      ON Products.ProductID = [Order Details].ProductID
Where DATEPART(YEAR, OrderDate) in 
            (Select DATEPART(YEAR, OrderDate) from Orders
			  where (DATEPART(YEAR, OrderDate) =1997))	
Group by products.ProductName
order by Summa desc 

--15
SELECT top 8 ProductName, Sum([Order Details].UnitPrice*Quantity) as Summa
   	  FROM Orders
	  join [Order Details]
	  on Orders.OrderID=[Order Details].OrderID
      JOIN Products
      ON Products.ProductID = [Order Details].ProductID
Where DATEPART(YEAR, OrderDate) in 
            (Select DATEPART(YEAR, OrderDate) from Orders
			  where (DATEPART(YEAR, OrderDate) =1997))	
Group by products.ProductName
order by Summa desc 

--16
Select CustomerID, Sum([Order Details].UnitPrice*Quantity) as Summa
from [Order Details]
Inner Join Orders
On [Order Details].OrderID=Orders.OrderID
Group by Orders.CustomerID
Having Sum([Order Details].UnitPrice*Quantity)>500
order by Summa desc 

--17
Select CustomerID, Sum([Order Details].UnitPrice*Quantity) as Summa
from [Order Details]
Inner Join Orders
On [Order Details].OrderID=Orders.OrderID
Group by Orders.CustomerID
Having Sum([Order Details].UnitPrice*Quantity)<1000
order by Summa desc 