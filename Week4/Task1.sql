create database Task1

Use Task1

Create table S
(
	n_post char(5) not null,
	name char(20),
	reiting smallint,
	town char(15)
)

Create table P
(
	n_det char(6),
	name char(20),
	cvet char(7),
	ves smallint,
	town char(15)
)

Create table SP
(
	n_post char(5),
	n_det char(6),
	date_post date,
	kol smallint
)

insert into S
(n_post, name, reiting, town)
values
('S1', 'Smitt', 20, 'London'),
('S2', 'Jones', 10, 'Paris'),
('S3', 'Bleik', 30, 'Paris'),
('S4', 'Klark', 20, 'London'),
('S5', 'Adams', 30, 'Atens')

insert into P
Values
('P1', 'Gaika', 'Red', 12, 'London'),
('P2', 'Bolt', 'Green', 17, 'Paris'),
('P3', 'Vint', 'Blue', 17, 'Rome'),
('P4', 'Vint', 'red', 14, 'London'),
('P5', 'Kulachok', 'Blue', 12, 'Paris'),
('P6', 'Blum', 'red', 19, 'London')

insert into SP
Values
('S1', 'P1', '02/01/95', 300),
('S1', 'P2', '04/05/95', 200),
('S1', 'P3', '05/12/95', 400),
('S1', 'P4', '06/15/95', 200),
('S1', 'P5', '07/22/95', 100),
('S1', 'P6', '08/13/95', 100),
('S2', 'P1', '03/03/95', 300),
('S2', 'P2', '06/12/95', 400),
('S3', 'P2', '06/12/95', 400),
('S4', 'P2', '03/23/95', 200),
('S4', 'P4', '06/17/95', 300),
('S4', 'P5', '08/22/95', 400)

--1
Select * from S

--2
Select name, town, reiting, n_post from S

--3
Select Distinct n_det from SP

--4
Select  LEFT(name , 2), reiting from S


Select Substring(name, 1,2), reiting from S

--5
Select * from S
Order by town, reiting

--6
select *from P
Where name like 'B%'

--7
select MAX(kol) as maximum, MIN(kol) as minimum,  AVG(kol) as average
from SP
where n_post ='S1'

--8
Select n_det, kol, 
DAY(date_post) day,  
MONTH(date_post) month,  
datepart(WEEKDAY, date_post ) weekday,
DATEDIFF(day,  date_post, GETDATE())   
from SP
 
--9
Select S.*, P.*
from S,P
Where S.town=P.town

--10
select n_det, sum(kol) as Sum 
from SP
where n_post !='S1'
Group by n_det

--11
select n_det
from P
Where ves >16 
Union 
Select n_det 
from SP
Where n_post = 'S2'

--12
delete from SP
where 'London'=
	(Select town from S
	 Where S.n_post=SP.n_post)
select * from SP